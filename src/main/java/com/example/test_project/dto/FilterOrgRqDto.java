package com.example.test_project.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class FilterOrgRqDto {
    @NotEmpty
    private String name;

    private Long inn;

    private boolean isActive;
}
