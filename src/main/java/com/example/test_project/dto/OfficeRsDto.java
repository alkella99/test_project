package com.example.test_project.dto;

import lombok.Data;

@Data
public class OfficeRsDto {

    private String name;

    private String address;

    private Long phone;

    private boolean isActive;
}
