package com.example.test_project.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class OfficeRqDto {
    private UUID uuid;

    private String name;

    private String address;

    private Long phone;

    private boolean isActive;

    private UUID organizationUuid;
}
