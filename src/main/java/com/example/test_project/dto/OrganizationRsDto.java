package com.example.test_project.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class OrganizationRsDto {
    private UUID uuid;

    private String name;

    private String fullName;

    private Long inn;

    private Long kpp;

    private String address;

    private Long phone;

    private boolean isActive;
}
