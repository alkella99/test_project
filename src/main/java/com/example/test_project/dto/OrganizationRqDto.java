package com.example.test_project.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class OrganizationRqDto {
    private UUID uuid;

    @NotEmpty(message = "Имя организации не может быть пустым")
    private String name;

    private String fullName;

    @NotNull
    private Long inn;

    private Long kpp;

    private String address;

    private Long phone;

    private boolean isActive;
}
