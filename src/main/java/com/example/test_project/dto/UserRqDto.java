package com.example.test_project.dto;

import lombok.Data;

@Data
public class UserRqDto {
    public String username;

    public String password;

    public String passwordConfirm;
}
