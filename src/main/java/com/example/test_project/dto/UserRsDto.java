package com.example.test_project.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class UserRsDto {
    private UUID uuid;

    private String username;
}
