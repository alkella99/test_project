package com.example.test_project.controllres;

import com.example.test_project.dto.OfficeRqDto;
import com.example.test_project.dto.OfficeRsDto;
import com.example.test_project.service.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/office")
public class OfficeController {
    @Autowired
    private OfficeService officeService;

    @PostMapping("/save")
    public OfficeRsDto addOffice(@RequestBody OfficeRqDto officeRqDto){
        return officeService.addOffice(officeRqDto);
    }

    @GetMapping("get_all")
    public List<OfficeRsDto> getAllOffices(){
        return officeService.getOffices();
    }

    @PostMapping("/get_list")
    public List<OfficeRsDto> getOfficeList(@RequestBody OfficeRqDto officeRqDto){
        return officeService.getOfficesList(officeRqDto);
    }
}
