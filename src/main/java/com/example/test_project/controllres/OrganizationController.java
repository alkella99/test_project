package com.example.test_project.controllres;

import com.example.test_project.dto.FilterOrgRqDto;
import com.example.test_project.dto.OrganizationRqDto;
import com.example.test_project.dto.OrganizationRsDto;
import com.example.test_project.entity.Organization;
import com.example.test_project.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/api/organisation")
public class OrganizationController {
    @Autowired
    private OrganizationService organizationService;

    @PostMapping("/save")
    public OrganizationRsDto addOrganization(@Validated @RequestBody OrganizationRqDto organizationRqDto) throws Exception {
        return organizationService.addOrganization(organizationRqDto);
    }

    @PostMapping("/list")
    public Set<Organization> getOrgList(@Validated @RequestBody FilterOrgRqDto filterOrgRqDto){
        return organizationService.getOrgList(filterOrgRqDto);
    }

    @GetMapping("/{uuid}")
    public Organization getById(@PathVariable UUID uuid) throws Exception{
        return organizationService.findByIdUuid(uuid);
    }

    @PatchMapping("/update")
    public OrganizationRsDto updateOrganization(@RequestBody OrganizationRqDto organizationRqDto) throws Exception{
        return organizationService.update(organizationRqDto);
    }
}
