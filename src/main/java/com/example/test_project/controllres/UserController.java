package com.example.test_project.controllres;

import com.example.test_project.dto.LoginRsDto;
import com.example.test_project.dto.UserRqDto;
import com.example.test_project.dto.UserRsDto;
import com.example.test_project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public UserRsDto addUser(@RequestBody UserRqDto userRqDto) throws Exception {
        return userService.saveUser(userRqDto);
    }

    @PostMapping("/login")
    public LoginRsDto login(){
        return new LoginRsDto();
    }
}
