package com.example.test_project.exception;

public class PasswordsNotMatch extends Exception{
    public PasswordsNotMatch(String errorMessage){
        super(errorMessage);
    }
}
