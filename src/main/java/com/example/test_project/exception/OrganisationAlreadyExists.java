package com.example.test_project.exception;

public class OrganisationAlreadyExists extends Exception{
    public OrganisationAlreadyExists(String errorMessage){
        super(errorMessage);
    }
}
