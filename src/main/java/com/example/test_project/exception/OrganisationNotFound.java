package com.example.test_project.exception;

public class OrganisationNotFound extends Exception{
    public OrganisationNotFound(String errorMessage){
        super(errorMessage);
    }
}
