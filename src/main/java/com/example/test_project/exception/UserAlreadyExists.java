package com.example.test_project.exception;

public class UserAlreadyExists extends Exception{
    public UserAlreadyExists(String errorMessage){
        super(errorMessage);
    }
}
