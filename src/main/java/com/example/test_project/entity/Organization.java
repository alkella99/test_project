package com.example.test_project.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@Table(name = "organization")
public class Organization {
    @Id
    private UUID uuid;

    private String name;

    private String fullName;

    private Long inn;

    private Long kpp;

    private String address;

    private Long phone;

    private boolean activeOrganization;

    @OneToMany(mappedBy = "organization")
    private Set<Office> offices;
}
