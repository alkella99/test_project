package com.example.test_project.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "offices")
public class Office {
    @Id
    private UUID uuid;

    private String name;

    private String address;

    private Long phone;

    private boolean isActive;

    @ManyToOne
    @JoinColumn(name="organization_uuid", nullable=false)
    private Organization organization;
}
