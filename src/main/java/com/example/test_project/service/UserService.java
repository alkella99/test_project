package com.example.test_project.service;

import com.example.test_project.dto.UserRqDto;
import com.example.test_project.dto.UserRsDto;
import com.example.test_project.entity.Role;
import com.example.test_project.entity.User;
import com.example.test_project.exception.PasswordsNotMatch;
import com.example.test_project.exception.UserAlreadyExists;
import com.example.test_project.repository.RoleRepository;
import com.example.test_project.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userFromDB = userRepository.findByUsername(username).orElse(new User());
        if(userFromDB.getUuid() == null){
            throw new UsernameNotFoundException("User not found");
        }
        return userFromDB;
    }

    public UserRsDto saveUser(UserRqDto userRqDto) throws Exception {
        User userFromDB = userRepository.findByUsername(userRqDto.getUsername()).orElse(new User());
        if (!(userFromDB.getUuid() == null)){
            throw new UserAlreadyExists("User already exists");
        }

        if(!userRqDto.getPassword().equals(userRqDto.getPasswordConfirm())){
            throw new PasswordsNotMatch("Password and password confirm does not match.");
        }

        User user = new User();
        user.setUuid(UUID.randomUUID());
        user.setUsername(userRqDto.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(userRqDto.getPassword()));
        user.setRoles(Collections.singleton(new Role(1l,"ROLE_USER")));
        userRepository.save(user);
        return modelMapper.map(user, UserRsDto.class);
    }
}
