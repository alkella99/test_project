package com.example.test_project.service;

import com.example.test_project.dto.FilterOrgRqDto;
import com.example.test_project.dto.OrganizationRqDto;
import com.example.test_project.dto.OrganizationRsDto;
import com.example.test_project.entity.Organization;

import java.util.Set;
import java.util.UUID;

public interface OrganizationService {
    OrganizationRsDto addOrganization(OrganizationRqDto organizationRqDto) throws Exception;

    Set<Organization> getOrgList(FilterOrgRqDto filterOrgRqDto);

    Organization findByIdUuid(UUID uuid) throws Exception;

    OrganizationRsDto update(OrganizationRqDto organizationRqDto) throws Exception;
}
