package com.example.test_project.service.serviceImpl;

import com.example.test_project.dto.FilterOrgRqDto;
import com.example.test_project.dto.OrganizationRqDto;
import com.example.test_project.dto.OrganizationRsDto;
import com.example.test_project.entity.Organization;
import com.example.test_project.exception.OrganisationAlreadyExists;
import com.example.test_project.exception.OrganisationNotFound;
import com.example.test_project.repository.OrganizationRepository;
import com.example.test_project.service.OrganizationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrganizationServiceImpl implements OrganizationService {
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public OrganizationRsDto addOrganization(OrganizationRqDto organizationRqDto) throws Exception {
        Organization organizationFromDB = organizationRepository.findOrganizationByInn(organizationRqDto.getInn()).orElse(new Organization());
        if (!(organizationFromDB.getUuid() == null)){
            throw new OrganisationAlreadyExists("Организация уже существует");
        }

        Organization organization = modelMapper.map(organizationRqDto, Organization.class);
        organization.setUuid(UUID.randomUUID());
        organizationRepository.save(organization);

        return modelMapper.map(organization, OrganizationRsDto.class);
    }

    @Override
    public Set<Organization> getOrgList(FilterOrgRqDto filterOrgRqDto){
        Set<Organization> organizations = organizationRepository.findOrganizationByNameLikeOrInn('%'+filterOrgRqDto.getName()+'%', filterOrgRqDto.getInn());
        return organizations.stream().filter(organization -> organization.isActiveOrganization() == filterOrgRqDto.isActive()).collect(Collectors.toSet());
    }

    @Override
    public Organization findByIdUuid(UUID uuid) throws Exception{
        Organization organization = organizationRepository.findByUuid(uuid).orElse(new Organization());
        if (organization.getUuid() == null){
            throw new OrganisationNotFound("Организация не найдена");
        }
        return organization;
    }

    @Override
    public OrganizationRsDto update(OrganizationRqDto organizationRqDto) throws Exception {
        Organization organization = organizationRepository.findByUuid(organizationRqDto.getUuid()).orElse(new Organization());
        if (organization.getUuid() == null){
            throw new OrganisationNotFound("Организация не найдена");
        }

        //organization = modelMapper.map(organizationRqDto, Organization.class);

        return modelMapper.map(organizationRepository.save(modelMapper.map(organizationRqDto, Organization.class)), OrganizationRsDto.class);
    }
}
