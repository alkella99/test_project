package com.example.test_project.service.serviceImpl;

import com.example.test_project.dto.OfficeRqDto;
import com.example.test_project.dto.OfficeRsDto;
import com.example.test_project.entity.Office;
import com.example.test_project.entity.Organization;
import com.example.test_project.repository.OfficeRepository;
import com.example.test_project.service.OfficeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OfficeServiceImpl implements OfficeService {
    @Autowired
    private OfficeRepository officeRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public OfficeRsDto addOffice(OfficeRqDto officeRqDto) {
        Office office = modelMapper.map(officeRqDto, Office.class);
        office.setUuid(UUID.randomUUID());
        return modelMapper.map(officeRepository.save(office), OfficeRsDto.class);
    }

    @Override
    public List<OfficeRsDto> getOffices() {
        List<Office> offices = officeRepository.findAll();
        return offices.stream().map(office -> modelMapper.map(office, OfficeRsDto.class)).collect(Collectors.toList());
    }

    @Override
    public List<OfficeRsDto> getOfficesList(OfficeRqDto officeRqDto) {
        List<Office> offices = officeRepository.findOfficeByOrganizationUuidAndNameAndPhone(officeRqDto.getOrganizationUuid(),
                officeRqDto.getName(), officeRqDto.getPhone());
        return offices.stream().map(office -> modelMapper.map(office, OfficeRsDto.class)).collect(Collectors.toList());
    }

    @Override
    public OfficeRsDto getOfficeById() {
        return null;
    }

    @Override
    public OfficeRsDto updateOffice(OfficeRqDto officeRqDto) {
        return null;
    }
}
