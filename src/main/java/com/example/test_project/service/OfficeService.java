package com.example.test_project.service;

import com.example.test_project.dto.OfficeRqDto;
import com.example.test_project.dto.OfficeRsDto;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface OfficeService {
    OfficeRsDto addOffice(OfficeRqDto officeRqDto);

    List<OfficeRsDto> getOffices();

    List<OfficeRsDto> getOfficesList(OfficeRqDto officeRqDto);

    OfficeRsDto getOfficeById();

    OfficeRsDto updateOffice(OfficeRqDto officeRqDto);
}
