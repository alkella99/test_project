package com.example.test_project.repository;

import com.example.test_project.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface OrganizationRepository extends JpaRepository<Organization, UUID> {
    Optional<Organization> findOrganizationByInn(Long inn);

    Set<Organization> findOrganizationByNameLikeOrInn(String name, Long inn);

    Optional<Organization> findByUuid(UUID uuid);
}
