package com.example.test_project.repository;

import com.example.test_project.entity.Office;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    List<Office> findOfficeByOrganizationUuidAndNameAndPhone(UUID orgId, String name, Long phone);
}
